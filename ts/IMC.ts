class IMC {
	private _height: number;
	private _weight: number;
	private _condition!: string;
	private _value!: number;

	constructor(height: number, weight: number) {
		this._height = height;
		this._weight = weight;
	}

	public get height(): number {
		return this._height;
	}

	public get weight(): number {
		return this._weight;
	}

	public get condition(): string {
		return this._condition;
	}

	public get value(): number {
		return this._value;
	}

	public set height(newHeight: number) {
		this._height = newHeight;
	}

	public set weight(newWeight: number) {
		this._weight = newWeight;
	}

	public set condition(newCondition: string) {
		this._condition = newCondition;
	}

	public set value(newValue: number) {
		this._value = newValue;
	}

	public calculateIMC(): void {
		const imc = this.weight / Math.pow(this.height, 2);
		const formattedImc = Math.round(imc * 100) / 100;
		this.updateCondition(formattedImc);

		this.value = formattedImc;
	}

	public updateCondition(imcValue: number): void {
		if (imcValue < 18.5) {
			this.condition = 'Magreza';
		} else if (imcValue >= 18.5 && imcValue <= 24.9) {
			this.condition = 'Normal';
		} else if (imcValue > 24.9 && imcValue <= 30) {
			this.condition = 'Sobrepeso';
		} else {
			this.condition = 'Obesidade';
		}
	}
}
