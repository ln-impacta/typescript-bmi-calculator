const weightSelector = document.querySelector<HTMLInputElement>(
	'#weight'
) as HTMLInputElement;
const heightSelector = document.querySelector<HTMLInputElement>(
	'#height'
) as HTMLInputElement;
const result = document.querySelector('#result') as HTMLElement;

// TODO: continue logic to add selected class based on user's result
// const tableInfo = document.querySelector('#info') as HTMLElement;
// const tablePrimary = document.querySelector('#primary') as HTMLElement;
// const tableWarning = document.querySelector('#warning') as HTMLElement;
// const tableDanger = document.querySelector('#danger') as HTMLElement;

// const tableRows = document.querySelectorAll('#tbody') as NodeList;
// const resetTableStyles = () => {};

const getValuesFromFields = () => {
	const weight: string = weightSelector.value;
	const height: string = heightSelector.value;

	getImc(parseFloat(height), parseFloat(weight));
};

const clearFields = () => {
	weightSelector.value = '';
	heightSelector.value = '';
	result.innerHTML = '';
};

const getImc = (height: number, weight: number) => {
	const imc = new IMC(height, weight);

	updateScreen(imc);
};

const updateScreen = (imc: IMC): void => {
	imc.calculateIMC();
	const { value, condition } = imc;
	const classStyle = setClassStyleBasedOnCondition(condition);

	result.innerHTML = `
		<div class="notification ${classStyle} is-light">
			<p>O seu <strong>IMC</strong> é <strong>${value}</strong> e a sua <strong>condição</strong> é <strong>${condition}</strong>.</p>
		</div>
	`;
};

const setClassStyleBasedOnCondition = (condition: string) => {
	switch (condition) {
		case 'Magreza':
			// tableInfo.classList.add('is-selected');
			// tableInfo.classList.add('imc-info');

			return 'is-info';
		case 'Normal':
			// tablePrimary.classList.add('is-selected');
			// tablePrimary.classList.add('imc-primary');

			return 'is-primary';
		case 'Sobrepeso':
			// tableWarning.classList.add('is-selected');
			// tableWarning.classList.add('imc-warning');

			return 'is-warning';
		case 'Obesidade':
			// tableDanger.classList.add('is-selected');
			// tableDanger.classList.add('imc-danger');

			return 'is-danger';
		default:
			break;
	}
};

window.onload = function () {
	document
		.querySelector<HTMLElement>('#submit-button')
		?.addEventListener('click', getValuesFromFields);
	document
		.querySelector<HTMLElement>('#cancel-button')
		?.addEventListener('click', clearFields);
};
