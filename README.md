# TypeScript BMI Calculator

Simple BMI calculator built with TypeScript and Bulma for styling.

### Running the project

This project uses TypeScript, so you'll need to transpile the `.ts` files first in order to see the HTML file working properly.

To do that, simply run:

`npm install`

followed by:

`npm run build`

This last command will generate your `dist` folder so you can simply open the `index.html` file in your browser and everything should be working fine.
